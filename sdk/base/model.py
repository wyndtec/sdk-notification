class BaseModel:

    _errors = None

    def __init__(self):
        self._errors = None

    def __str__(self):
        return str(self.to_dict())
        return str({k: str(v) for k, v in self.to_dict().items()})

    def __repr__(self):
        return str(self.to_dict())

    def validate(self):
        self.not_implement

    def is_valid(self):
        """ Retorna True caso o model seja valido e false caso não seja """

        if self.errors is None:
            self.validate()

        return not bool(self.errors)

    @property
    def errors(self):
        return self._errors

    def add_error(self, error_msg):
        """ Adiciona novos erros ao model """
        if self._errors is None:
            self._errors = []
        self._errors.append(error_msg)

    def to_dict(self, sync=False):
        """ Transaforma o model em um dict """
        self.not_implement

    def __iter__(self):
        data = self.to_dict()

        for k in data:
            yield k, data[k]

    @classmethod
    def create_object(cls, data: dict):
        """ Cria um novo objeto com os valores passados """

        return cls(**data)

    @property
    def not_implement(self):
        raise NotImplementedError

    pk = not_implement
