from sdk.connection import Request
from sdk.util import concat_url
from sdk.base.model import BaseModel


class BaseSync:

    _endpoint = None  # Endpoint usado
    _model = None  # Model que trata os dados

    def __init__(self):
        pass

    def get_endpoint(self):
        return self._endpoint

    def get_model(self):
        return self._model

    def list(self, params: dict = dict()):
        """
        Lista todos os dados cadastrados
        :param params: Parametros extras da requisição
        :return: Lista de objetos retornados
        """
        request = Request()
        response = request.get(endpoint=self.get_endpoint(), params=params)

        if response.ok:
            return [self.get_model().create_object(data) for data in response.json()['results']]
        else:
            raise Exception(response.text)

    def retrieve(self, uuid_register: str):
        """
        Retorna os detalhes de um objeto
        :param uuid_register: UUID do registro a ser consultado
        :return: Objeto consultado
        """
        request = Request()
        response = request.get(endpoint=concat_url(self.get_endpoint(), uuid_register))
        return self.get_model().create_object(response)

    def create(self, model):
        """
        Cria um novo registro
        :param model: Model preenchidos com os dados a serem enviados
        :return: Objeto com os dados do novo cadastro
        """

        if not isinstance(model, BaseModel):
            raise TypeError('Param model informado inválido!')

        if not model.is_valid():
            raise ValueError(model.errors)

        request = Request()
        response = request.post(endpoint=self.get_endpoint(), data=model.to_dict(sync=True))
        return self.get_model().create_object(response)

    def update(self, model):
        """
        Atualiza os dados de um registro
        :param uuid_register: UUID do registro a ser atualizado
        :param data: Dados a serem enviados
        :return:
        """

        if not isinstance(model, BaseModel):
            raise TypeError('Param model informado inválido!')

        if not model.is_valid():
            raise ValueError(model.errors)

        if not model.pk:
            raise ValueError('PK do model não informado!')

        request = Request()
        response = request.put(
            endpoint=concat_url(self.get_endpoint(), model.pk),
            data=model.to_dict(sync=True)
        )
        return response

    def delete(self, uuid_register: str):
        """
        Delete um registro
        :param uuid_register: UUID do registro a ser apagado
        :return: True se deu certo
        """
        request = Request()
        response = request.delete(endpoint=concat_url(self.get_endpoint(), uuid_register))
        return response
