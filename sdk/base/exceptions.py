class BaseWarning(Warning):
    """
    Base warning class for warnings.
    """


class PerformanceWarning(BaseWarning):
    """
    Warning raised when there's a possible performance impact.
    """


class BaseError(Exception):
    """
    Base exception class.
    """


class AuthError(BaseError):
    """
    Invalid authentication details.
    """
    def __init__(self):
        super(AuthError, self).__init__('Invalid authentication details')


class ServerError(BaseError):
    """
    Internal error.
    """
    def __init__(self):
        super(ServerError, self).__init__('Returned internal error')


class RequestEntityTooLargeError(BaseError):
    """
    Size of the request exceeds the capacity limit on the server.
    """
    def __init__(self):
        super(RequestEntityTooLargeError, self).__init__(
            "The requested resource doesn't allow POST requests or the size of the request exceeds the capacity limit")


class UnknownError(BaseError):
    """
    Returned unknown error.
    """
    def __init__(self, status_code):
        self.status_code = status_code
        super(UnknownError, self).__init__(
            'Returned unknown error with the status code {0}'.format(status_code))


class ValidationError(BaseError):
    """
    Validation errors occured on create/update resource.
    """
    def __init__(self, error):
        super(ValidationError, self).__init__(error)


class ForbiddenError(BaseError):
    """
    Requested resource is forbidden.
    """
    def __init__(self):
        super(ForbiddenError, self).__init__('Requested resource is forbidden')


class ResourceNotFoundError(BaseError):
    """
    Requested resource doesn't exist.
    """
    def __init__(self):
        super(ResourceNotFoundError, self).__init__("Requested resource doesn't exist")


class JSONDecodeError(BaseError):
    """
    Unable to decode received JSON.
    """
    def __init__(self, response):
        self.response = response
        super(JSONDecodeError, self).__init__(
            'Unable to decode received JSON, you can inspect exception\'s '
            '"response" attribute to find out what the response was')


class ConflictError(BaseError):
    """
    Resource version on the server is newer than on the client.
    """
    def __init__(self):
        super(ConflictError, self).__init__('Resource version on the server is newer than on the client')
