import uuid


def concat_url(base, endpoint):
    """
    Concatena a URL
    :param endpoint: Endpoint do servidor a ser consultado
    :return: URL completa a ser usada com o servidor e o endpoint
    """
    if endpoint.startswith('/'):
        endpoint = endpoint[1:]

    if base.endswith('/'):
        url = f'{base}{endpoint}'
    else:
        url = f'{base}/{endpoint}'

    if not url.endswith('/'):
        url = f'{url}/'

    return url


def is_uuid_valid(uuid_str: str):
    """
    Valida se a string passada é uma uuid válida
    :param uuid_str: Str com ovalor do uuid
    :return: True se for válido, False caso não seja
    """
    if uuid_str:
        try:
            uuid.UUID(uuid_str)
            return True
        except ValueError:
            return False
    else:
        return False
