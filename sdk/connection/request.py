import requests
import json

from decouple import config
from .connection import Connection
from requests.auth import HTTPBasicAuth

from sdk.util import concat_url

from sdk.base import exceptions

USERNAME = config('NOTIFICATION_USERNAME')
PASSWORD = config('NOTIFICATION_PASSWORD')


class Request:
    """"
    Classe que irá tratar as requisições com o servidor
    """

    def __init__(self):
        self._connection = None
        self._request_auth = None
        self._namespace = config('NOTIFICATION_NAMESPACE')

        self.authenticate(USERNAME, PASSWORD)

    @property
    def connection(self):
        if self._connection is None:
            self._connection = Connection()
        return self._connection

    def authenticate(self, username: str, password: str):
        self._request_auth = HTTPBasicAuth(username, password)
        return self._request_auth

    def get_header(self):
        return {
            'Content-Type': 'application/json'
        }

    def _request(self, method: str, endpoint: str, data: dict = None,
                 headers: dict = None, params: dict = None):
        """
        Faz uma requisição
        :param method: Método do verbo http a ser utilizado (get, post...)
        :param endpoint: Endpoint do servidor a ser consultado
        :param data: Dados a serem enviados na requisição
        :param headers: Headers da requisição
        :param params: URL parametros
        :return: requests.Response
        """

        if not self._request_auth:
            raise Exception('You Must authenticate before fetch data.')

        if not headers:
            headers = self.get_header()

        req = getattr(requests, method)

        response = req(
            url=concat_url(self.connection.server_url, endpoint),
            headers=headers,
            data=data,
            auth=self._request_auth,
            params=params
        )

        return self.process_response(response)

    def get(self, endpoint, headers: dict = None, params: dict = dict()):
        """
        Faz um requisição GET no servidor
        :param endpoint: Endpoint do servidor a ser consultado
        :param headers: Headers da requisição
        :param params: URL parametros
        :return: requests.Response
        """
        params.setdefault('namespace', self._namespace)
        return self._request(
            method='get', endpoint=endpoint, headers=headers, params=params
        )

    def post(self, endpoint, data: dict, headers: dict = None, params: dict = dict()):
        """
        Faz um requisição POST no servidor
        :param endpoint: Endpoint do servidor a ser consultado
        :param data: Dados do payload a serem enviados para o servidor
        :param headers: Headers da requisição
        :param params: URL parametros
        :return: requests.Response
        """
        params.setdefault('namespace', self._namespace)
        return self._request(
            method='post',
            endpoint=endpoint,
            headers=headers,
            params=params,
            data=json.dumps(data)
        )

    def put(self, endpoint, data: dict, headers: dict = None, params: dict = dict()):
        """
        Faz um requisição PUT no servidor
        :param endpoint: Endpoint do servidor a ser consultado
        :param data: Dados do payload a serem enviados para o servidor
        :param headers: Headers da requisição
        :param params: URL parametros
        :return: requests.Response
        """
        params.setdefault('namespace', self._namespace)
        return self._request(
            method='put',
            endpoint=endpoint,
            headers=headers,
            params=params,
            data=json.dumps(data)
        )

    def delete(self, endpoint, headers: dict = None, params: dict = dict()):
        """
        Faz um requisição PUT no servidor
        :param endpoint: Endpoint do servidor a ser consultado
        :param headers: Headers da requisição
        :param params: URL parametros
        :return: requests.Response
        """
        params.setdefault('namespace', self._namespace)
        return self._request(
            method='delete',
            endpoint=endpoint,
            headers=headers,
            params=params
        )


    def process_response(self, response):
        """
        Processas as respostas recebidas do servidor de notificações
        :param obj response: (required). Objeto com detalhes da resposta.
        """

        status_code = response.status_code

        if status_code in (200, 201, 204):
            if not response.content.strip():
                return True
            else:
                try:
                    return response.json()
                except (ValueError, TypeError):
                    raise exceptions.JSONDecodeError(response)
        elif status_code == 401:
            raise exceptions.AuthError
        elif status_code == 403:
            raise exceptions.ForbiddenError
        elif status_code == 404:
            raise exceptions.ResourceNotFoundError
        elif status_code == 409:
            raise exceptions.ConflictError
        elif status_code == 413:
            raise exceptions.RequestEntityTooLargeError
        elif status_code == 422:
            errors = response.json()['errors']
            raise exceptions.ValidationError(', '.join(': '.join(e) if isinstance(e, list) else e for e in errors))
        elif status_code == 500:
            raise exceptions.ServerError

        raise exceptions.UnknownError(status_code)
