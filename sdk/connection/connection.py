from decouple import config
from urllib.parse import urlparse, urljoin


class Connection:
    """ Classe para tratar as informações de conexão com o servidor """

    NOTIFICATION_SERVER = config('NOTIFICATION_SERVER', default='localhost:8000')

    def __init__(self, api_version: str = 'v1'):
        self._server_url = None
        self._api_version = api_version

    def get_url(self):
        return self._server_url

    @property
    def server_url(self):
        if self._server_url is None:
            url = urlparse(url=f'//{self.NOTIFICATION_SERVER}', scheme='http').geturl()
            self._server_url = urljoin(base=url, url=self._api_version)

        return self._server_url
