from decouple import config

from datetime import datetime

from sdk.util import is_uuid_valid
from sdk.base.model import BaseModel
from sdk.notification.models.namespace import Namespace
from sdk.notification.models.group import Group


class Subscriber(BaseModel):

    user_id = None
    name = None
    groups = None
    active = None
    created_at = None
    updated_at = None
    namespace = None

    def __init__(self, user_id: str = str(),
                 name: str = str(),
                 groups: list = [],
                 active: bool = False, **kwargs):

        self.user_id = user_id
        self.name = name
        self.active = active
        self.groups = []

        if kwargs.get('created_at'):
            self.created_at = datetime.fromtimestamp(int(kwargs.get('created_at')))
        if kwargs.get('updated_at'):
            self.updated_at = datetime.fromtimestamp(int(kwargs.get('updated_at')))

        if kwargs.get('namespace'):
            self.namespace = Namespace.create_object(kwargs.get('namespace'))
        else:
            self.namespace = Namespace.create_object(data={'pk': config('NOTIFICATION_NAMESPACE')})

        if groups:
            if not isinstance(groups, list):
                raise TypeError('Groups não é uma lista válida!')

            for group in groups:
                self.groups.append(Group.create_object(group))

    @property
    def user_id(self):
        return self.__user_id

    @user_id.setter
    def user_id(self, value):
        self.__user_id = value

    pk = user_id

    def validate(self):
        if not self.user_id or not is_uuid_valid(self.user_id):
            self.add_error('user_id informado é inválido!')

        if not self.name or not isinstance(self.name, str):
            self.add_error('O valor informado em name é inválido')

        if not isinstance(self.active, bool):
            self.add_error('O valor informado em active é inválido')

        if self.groups:
            if not isinstance(self.groups, list()):
                self.add_error('Grupos informados são inválidos!')
            else:
                for group in self.groups:
                    if not is_uuid_valid(group):
                        self.add_error(f'Grupo {group} é inválidos!')

    def to_dict(self, sync=False):
        """
        Convert o model em dict
        :param sync: Caso true, transformará os objetos em str
        :return:
        """
        namespace = self.namespace
        created_at = self.created_at
        updated_at = self.updated_at
        groups = self.groups

        if sync:
            if isinstance(self.namespace, Namespace):
                namespace = self.namespace.pk

            if groups:
                groups = []

                for group in groups:
                    if isinstance(group, Group):
                        group.append(group.pk)

        return {
            'user_id': self.user_id,
            'name': self.name,
            'namespace': namespace,
            'groups': groups,
            'created_at': str(created_at),
            'updated_at': str(updated_at)
        }
