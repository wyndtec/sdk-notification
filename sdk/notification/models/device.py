from datetime import datetime

from sdk.base.model import BaseModel
from sdk.notification.models.subscriber import Subscriber


class Device(BaseModel):

    pk = None
    active = None
    created_at = None
    updated_at = None
    subscriber = None
    name = None
    broker_id = None
    device_type = None
    model = None
    unique_id = None
    brand = None
    os_build_number = None
    os_version = None
    os_bundle_id = None
    os_readable_version = None
    android_fingerprint = None
    android_install_time = None
    android_bootloader = None
    ios_device_token = None

    def __init__(self,
                 subscriber: Subscriber = None,
                 name: str = str(),
                 broker_id: str = str(),
                 device_type: str = str(),
                 model: str = str(),
                 unique_id: str = str(),
                 brand: str = str(),
                 os_build_number: str = str(),
                 os_version: str = str(),
                 os_bundle_id: str = str(),
                 os_readable_version: str = str(),
                 active: bool = False,
                 **kwargs):

        self.name = name
        self.broker_id = broker_id
        self.device_type = device_type
        self.model = model
        self.unique_id = unique_id
        self.brand = brand
        self.os_build_number = os_build_number
        self.os_version = os_version
        self.os_bundle_id = os_bundle_id
        self.os_readable_version = os_readable_version
        self.active = active

        self.pk = kwargs.get('pk', None)
        self.android_fingerprint = kwargs.get('android_fingerprint', None)
        self.android_install_time = kwargs.get('android_install_time', None)
        self.android_bootloader = kwargs.get('android_bootloader', None)
        self.ios_device_token = kwargs.get('ios_device_token', None)

        if kwargs.get('created_at'):
            self.created_at = datetime.fromtimestamp(int(kwargs.get('created_at')))
        if kwargs.get('updated_at'):
            self.updated_at = datetime.fromtimestamp(int(kwargs.get('updated_at')))

        if subscriber:
            if isinstance(subscriber, str):
                self.subscriber = Subscriber.create_object(data={'user_id': subscriber})
            else:
                self.subscriber = Subscriber.create_object(subscriber)
        else:
            self.subscriber = Subscriber(kwargs.get('subscriber'))

    def validate(self):
        if not self.name or not isinstance(self.name, str):
            self.add_error('O valor informado em name é inválido')

        if not isinstance(self.active, bool):
            self.add_error('O valor informado em active é inválido')

        if not self.broker_id or not isinstance(self.broker_id, str):
            self.add_error('O valor informado em broker_id é inválido')

        if not self.device_type or not isinstance(self.device_type, str):
            self.add_error('O valor informado em device_type é inválido')

        if not self.model or not isinstance(self.model, str):
            self.add_error('O valor informado em model é inválido')

        if not self.unique_id or not isinstance(self.unique_id, str):
            self.add_error('O valor informado em unique_id é inválido')

        if not self.brand or not isinstance(self.brand, str):
            self.add_error('O valor informado em brand é inválido')

        if not self.os_build_number or not isinstance(self.os_build_number, str):
            self.add_error('O valor informado em os_build_number é inválido')

        if not self.os_version or not isinstance(self.os_version, str):
            self.add_error('O valor informado em os_version é inválido')

        if not self.os_bundle_id or not isinstance(self.os_bundle_id, str):
            self.add_error('O valor informado em os_bundle_id é inválido')

        if not self.os_readable_version or not isinstance(self.os_readable_version, str):
            self.add_error('O valor informado em os_readable_version é inválido')

        if not self.subscriber:
            self.add_error('O valor informado em subscriber é inválido')

    def to_dict(self, sync=False):
        """
        Convert o model em dict
        :param sync: Caso true, transformará os objetos em str
        :return:
        """

        subscriber = self.subscriber

        if sync:
            if isinstance(self.subscriber, Subscriber):
                subscriber = self.subscriber.pk

        return {
            'pk': self.pk,
            'active': self.active,
            'subscriber': subscriber,
            'name': self.name,
            'broker_id': self.broker_id,
            'device_type': self.device_type,
            'model': self.model,
            'unique_id': self.unique_id,
            'brand': self.brand,
            'os_build_number': self.os_build_number,
            'os_version': self.os_version,
            'os_bundle_id': self.os_bundle_id,
            'os_readable_version': self.os_readable_version,
            'android_fingerprint': self.android_fingerprint,
            'android_install_time': self.android_install_time,
            'android_bootloader': self.android_bootloader,
            'ios_device_token': self.ios_device_token,
            'created_at': str(self.created_at),
            'updated_at': str(self.updated_at)
        }
