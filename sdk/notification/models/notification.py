from datetime import datetime

from decouple import config

from sdk.base.model import BaseModel
from sdk.constants import NOTIFICATION_LANG_EN, NOTIFICATION_LANG_PT
from sdk.notification.models.subscriber import Subscriber
from sdk.notification.models.namespace import Namespace
from sdk.notification.models.group import Group


class Notification(BaseModel):

    pk = None
    created_at = None
    updated_at = None
    namespace = None
    language = None
    title = None
    url = None
    type = None
    broker_id = None
    text = None
    groups = []
    subscriber = None
    extra_data = None

    def __init__(self,
                 subscriber: Subscriber = None,
                 language: str = str(),
                 title: str = str(),
                 url: str = str(),
                 type: str = str(),
                 text: str = str(),
                 groups: str = str(),
                 extra_data: str = str(),
                 **kwargs):

        self.language = language
        self.title = title
        self.url = url
        self.type = type
        self.text = text
        self.groups = []
        self.extra_data = extra_data
        self.pk = kwargs.get('pk', None)

        if kwargs.get('created_at'):
            self.created_at = datetime.fromtimestamp(int(kwargs.get('created_at')))
        if kwargs.get('updated_at'):
            self.updated_at = datetime.fromtimestamp(int(kwargs.get('updated_at')))

        if kwargs.get('namespace'):
            self.namespace = Namespace.create_object(kwargs.get('namespace'))
        else:
            self.namespace = Namespace.create_object(data={'pk': config('NOTIFICATION_NAMESPACE')})

        if subscriber:
            if isinstance(subscriber, str):
                self.subscriber = Subscriber.create_object(data={'user_id': subscriber})
            else:
                self.subscriber = Subscriber.create_object(subscriber)
        else:
            self.subscriber = Subscriber(kwargs.get('subscriber'))

        if groups:
            if not isinstance(groups, list):
                raise TypeError('Groups não é uma lista válida!')

            for group in groups:
                self.groups.append(Group.create_object(group))

    def validate(self):
        if self.language not in(NOTIFICATION_LANG_EN, NOTIFICATION_LANG_PT):
            self.add_error('O valor informado em language é inválido')

        if not self.title or not isinstance(self.title, str):
            self.add_error('O valor informado em title é inválido')

        if not self.url or not isinstance(self.url, str):
            self.add_error('O valor informado em url é inválido')

        if not self.type or not isinstance(self.type, str):
            self.add_error('O valor informado em type é inválido')

        if not self.text or not isinstance(self.text, str):
            self.add_error('O valor informado em text é inválido')

        if not self.subscriber and not self.groups:
            self.add_error('É necessário informar o destinatário da notificação!')

    def to_dict(self, sync=False):
        """
        Convert o model em dict
        :param sync: Caso true, transformará os objetos em str
        :return:
        """

        subscriber = self.subscriber
        namespace = self.namespace
        groups = self.groups

        if sync:
            if isinstance(self.subscriber, Subscriber):
                subscriber = self.subscriber.pk
            if isinstance(self.namespace, Namespace):
                namespace = self.namespace.pk

            if groups:
                groups = []

                for group in groups:
                    if isinstance(group, Group):
                        group.append(group.pk)

        return {
            'pk': self.pk,
            'namespace': namespace,
            'language': self.language,
            'title': self.title,
            'url': self.url,
            'type': self.type,
            'broker_id': self.broker_id,
            'text': self.text,
            'groups': groups,
            'subscriber': subscriber,
            'extra_data': self.extra_data,
            'created_at': str(self.created_at),
            'updated_at': str(self.updated_at)
        }
