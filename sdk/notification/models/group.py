from datetime import datetime

from decouple import config

from sdk.base.model import BaseModel
from sdk.notification.models.namespace import Namespace


class Group(BaseModel):

    name = None
    alias = None
    namespace = None
    active = None
    pk = None
    created_at = None
    updated_at = None

    def __init__(self,
                 name: str = str(),
                 alias: str = str(),
                 active: bool = False,
                 **kwargs):

        self.name = name
        self.alias = alias
        self.active = active

        if kwargs.get('created_at'):
            self.created_at = datetime.fromtimestamp(int(kwargs.get('created_at')))
        if kwargs.get('updated_at'):
            self.updated_at = datetime.fromtimestamp(int(kwargs.get('updated_at')))
        if kwargs.get('pk'):
            self.pk = kwargs.get('pk')
        if kwargs.get('namespace'):
            self.namespace = Namespace.create_object(kwargs.get('namespace'))
        else:
            self.namespace = Namespace.create_object(data={'pk': config('NOTIFICATION_NAMESPACE')})

    def validate(self):
        if not self.name or not isinstance(self.name, str):
            self.add_error('O valor informado em name é inválido')

        if not self.alias or not isinstance(self.alias, str):
            self.add_error('alias informado é inválido!')

        if not isinstance(self.active, bool):
            self.add_error('O valor informado em active é inválido')

    def to_dict(self, sync=False):
        """
        Convert o model em dict
        :param sync: Caso true, transformará os objetos em str
        :return:
        """

        namespace = self.namespace

        if sync:
            if isinstance(self.namespace, object):
                namespace = self.namespace.pk

        return {
            'name': self.name,
            'alias': self.alias,
            'namespace': namespace,
            'active': self.active,
            'pk': self.pk,
            'created_at': str(self.created_at),
            'updated_at': str(self.updated_at),
        }
