from datetime import datetime

from sdk.base.model import BaseModel

from sdk.constants import BROKER_TYPE_ONESIGNAL


class Namespace(BaseModel):

    name = None
    description = None
    external_id = None
    broker_type = None
    broker_app_id = None
    broker_api_key = None
    last_process = None
    active = None
    pk = None
    created_at = None
    updated_at = None

    def __init__(self,
                 name: str = str(),
                 description: str = str(),
                 broker_type: str = str(),
                 broker_app_id: str = str(),
                 broker_api_key: str = str(),
                 active: bool = False,
                 **kwargs):

        self.name = name
        self.description = description
        self.broker_type = broker_type
        self.broker_app_id = broker_app_id
        self.broker_api_key = broker_api_key
        self.active = active

        if kwargs.get('created_at'):
            self.created_at = datetime.fromtimestamp(int(kwargs.get('created_at')))
        if kwargs.get('updated_at'):
            self.updated_at = datetime.fromtimestamp(int(kwargs.get('updated_at')))
        if kwargs.get('external_id'):
            self.external_id = kwargs.get('external_id')
        if kwargs.get('last_process'):
            self.last_process = datetime.fromtimestamp(int(kwargs.get('last_process')))
        if kwargs.get('pk'):
            self.pk = kwargs.get('pk')

    def validate(self):
        if not self.name or not isinstance(self.name, str):
            self.add_error('O valor informado em name é inválido')

        if not self.broker_app_id or not isinstance(self.broker_app_id, str):
            self.add_error('broker_app_id informado é inválido!')

        if not self.broker_api_key or not isinstance(self.broker_api_key, str):
            self.add_error('broker_api_key informado é inválido!')

        if self.broker_type != BROKER_TYPE_ONESIGNAL:
            self.add_error('broker_type inválido!')

    def to_dict(self, sync=False):
        return {
            'name': self.name,
            'description': self.description,
            'external_id': self.external_id,
            'broker_type': self.broker_type,
            'broker_app_id': self.broker_app_id,
            'broker_api_key': self.broker_api_key,
            'last_process': self.last_process,
            'active': self.active,
            'pk': self.pk,
            'created_at': str(self.created_at),
            'updated_at': str(self.updated_at),
        }
