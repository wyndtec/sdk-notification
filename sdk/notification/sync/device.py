from sdk.base.sync import BaseSync
from sdk.notification.models import Device, Subscriber


class DeviceSync(BaseSync):

    _endpoint = "/notification/devices"
    _model = Device

    def list(self, subscribe):
        """
        Lista todos os dispositivos de um subscribe
        :param subscribe: Pk do subscribe ou o objeto subscribe
        """
        sub = subscribe
        if isinstance(subscribe, Subscriber):
            sub = subscribe.pk

        return super(DeviceSync, self).list(params={'subscriber': sub})
