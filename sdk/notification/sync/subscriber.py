from sdk.base.sync import BaseSync
from sdk.notification.models import Subscriber


class SubscriberSync(BaseSync):

    _endpoint = '/notification/subscribers'
    _model = Subscriber
