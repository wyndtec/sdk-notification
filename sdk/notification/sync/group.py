from decouple import config

from sdk.base.sync import BaseSync
from sdk.notification.models import Group


class GroupSync(BaseSync):

    _endpoint = f"/notification/namespaces/{config('NOTIFICATION_NAMESPACE')}/groups"
    _model = Group
