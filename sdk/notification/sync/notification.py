from sdk.base.sync import BaseSync
from sdk.notification.models import Notification


class NotificationSync(BaseSync):

    _endpoint = '/notification/notifications'
    _model = Notification
