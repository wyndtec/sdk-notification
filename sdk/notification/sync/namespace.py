from sdk.base.sync import BaseSync
from sdk.notification.models import Namespace


class NamespaceSync(BaseSync):

    _endpoint = '/notification/namespaces'
    _model = Namespace
