import os

import setuptools

README_FILE = os.path.join(os.path.dirname(__file__), 'README.md')
REQUIREMENTS_FILE = os.path.join(os.path.dirname(__file__), 'requirements.txt')
VERSION_FILE = os.path.join(os.path.dirname(__file__), 'VERSION')
DEFAULT_VERSION = '0.0.1'


def get_description():
    # When running tests using tox, README.md is not found
    desc = ''
    try:
        with open(README_FILE) as file:
            desc = file.read()
    except FileNotFoundError:
        pass

    return desc


def get_version():
    with open(VERSION_FILE) as f:
        git_version = f.read().strip()
        if not git_version:
            raise Exception('Versão não definida')
        git_version = git_version.split('-')
        version = git_version[0]
        if len(git_version) > 1:
            version += '.post' + git_version[1]
        return version


def get_requirements():
    with open(REQUIREMENTS_FILE) as f:
        requirements = f.read().splitlines()

    return requirements


setuptools.setup(
    name="sdk-notification-service",
    # version=get_version(),
    version=0.1,
    author="Hugo Seabra",
    author_email="dev@wynd.com",
    description="Library of synchronizer Sankhya ERP and convert XML to dict",
    long_description=get_description(),
    # install_requires=get_requirements(),
    install_requires=[
        'requests',
        'python-decouple'
    ],
    long_description_content_type="text/markdown",
    url="",
    license='MIT',
    packages=setuptools.find_packages(),
    classifiers=[
        'Development Status :: 0 - Production/Stable',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        "Operating System :: OS Independent",
    ],
    keywords='',
    python_requires='>=3.6',
    zip_safe=False,
    package_data={},
)

